const BALL_WIDTH = 20;
const BALL_HEIGHT = 20;

const PLATE_WIDTH = 110;
const PLATE_HEIGHT = 10;

const INITIAL_LIVES = 3;

let plateElement = null;
let points = 0;
let lives = 0;

let intervalRef;

window.addEventListener('load', () => {
    startGame();
});

function handleArrowKeyPress(e) {
    if (e.key.toLowerCase() === 'arrowright') {
        plateElement.moveRight();
    } else if (e.key.toLowerCase() === 'arrowleft') {
        plateElement.moveLeft();
    }
}

function handleFwdBtnPress() {
    plateElement.moveRight();
    plateElement.moveRight();
}

function handleBackBtnPress() {
    plateElement.moveLeft();
    plateElement.moveLeft();
}

function startGame() {
    clearInterval(intervalRef);
    points = 0;
    lives = INITIAL_LIVES;

    window.removeEventListener('keydown', handleArrowKeyPress);
    getBackBtn().removeEventListener('click', handleBackBtnPress);
    getFwdBtn().removeEventListener('click', handleFwdBtnPress);
    hideMessage();

    const ball = new Ball(600, 500, bottomHitAction);
    plateElement = new Plate(600, 500);

    getResultsSpan().textContent = points;
    getLivesSpan().textContent = lives;

    window.addEventListener('keydown', handleArrowKeyPress);
    getBackBtn().addEventListener('click', handleBackBtnPress);
    getFwdBtn().addEventListener('click', handleFwdBtnPress);

    intervalRef = setInterval(() => ball.move(), 10);
}

function bottomHitAction(positionX) {
    const plateRange = plateElement.getPositionXRange();
    if (positionX >= plateRange.from && positionX <= plateRange.to) {
        getResultsSpan().textContent = ++points;
    } else {
        getLivesSpan().textContent = --lives;
        if (lives == 0) {
            displayMessage('MÄNG LÄBI!');
            clearInterval(intervalRef);
        }
    }
}

class Ball {
    constructor(canvasWidth, canvasHeight, bottomHitCallback) {
        this._canvasWidth = canvasWidth;
        this._canvasHeight = canvasHeight;
        this._bottomHitCallback = bottomHitCallback;

        this._speedX = parseInt(Math.random() * 6);
        this._speedY = parseInt(Math.random() * 6);

        this._directionX = Math.random() > 0.5 ? 1 : -1;
        this._directionY = Math.random() > 0.5 ? 1 : -1;

        this._positionX = parseInt((this._canvasWidth - BALL_WIDTH) / 2);
        this._positionY = parseInt((this._canvasHeight - BALL_HEIGHT) / 2);

        this._ballElement = document.querySelector('#ball');
        this._ballElement.style.width = `${BALL_WIDTH}px`;
        this._ballElement.style.height = `${BALL_HEIGHT}px`;
        this._render();
    }

    move() {
        this._positionX = this._positionX + this._speedX * this._directionX;
        this._positionY = this._positionY + this._speedY * this._directionY;

        if (this._isOffLeft() || this._isOffRight()) {
            this._directionX = this._directionX * -1;
        }

        if (this._isOffTop() || this._isOffBottom()) {
            this._directionY = this._directionY * -1;
        }

        if (this._isOffBottom()) {
            this._bottomHitCallback(this._positionX);
        }

        this._render();
    }

    _render() {
        this._ballElement.style.left = `${this._positionX}px`;
        this._ballElement.style.top = `${this._positionY}px`;
    }

    _isOffLeft = () => this._positionX <= 0;
    _isOffRight = () => this._positionX >= this._canvasWidth - BALL_WIDTH;
    _isOffTop = () => this._positionY <= 0;
    _isOffBottom = () => this._positionY >= this._canvasHeight - BALL_HEIGHT;
}

class Plate {
    constructor(canvasWidth, canvasHeight) {
        this._canvasWidth = canvasWidth;
        this._canvasHeight = canvasHeight;

        this._speedX = 20;

        this._positionX = parseInt((this._canvasWidth - PLATE_WIDTH) / 2);

        this._plateElement = getPlate();
        this._plateElement.style.width = `${PLATE_WIDTH}px`;
        this._plateElement.style.height = `${PLATE_HEIGHT}px`;
        this._plateElement.style.bottom = 0;
        this._render();
    }

    moveRight() {
        this._positionX = this._positionX + this._speedX;
        if (this._positionX + PLATE_WIDTH >= this._canvasWidth) {
            this._positionX = this._canvasWidth - PLATE_WIDTH;
        }
        this._render();
    }

    moveLeft() {
        this._positionX = this._positionX - this._speedX;
        if (this._positionX < 0) {
            this._positionX = 0;
        }
        this._render();
    }

    getPositionXRange() {
        return {
            from: this._positionX,
            to: this._positionX + PLATE_WIDTH
        }
    }

    _render() {
        this._plateElement.style.left = `${this._positionX}px`;
    }
}

const displayMessage = text => {
    getMessageDiv().style.display = 'flex';
    getMessageDiv().textContent = text;
}
const hideMessage = () => getMessageDiv().style.display = 'none';

const getPlate = () => document.querySelector('#plate');
const getResultsSpan = () => document.querySelector('#results span');
const getLivesSpan = () => document.querySelector('#lives span');
const getBackBtn = () => document.querySelector('#buttons div:nth-child(1)');
const getFwdBtn = () => document.querySelector('#buttons div:nth-child(2)');
const getMessageDiv = () => document.querySelector('#message');